https://www.teracloud.io/single-post/self-managed-argocd-wait-argocd-can-manage-itself
```
helm repo add argo https://argoproj.github.io/argo-helm
kubectl create namespace argocd
helm install argo argo/argo-cd -n argocd


kubectl port-forward service/argo-argocd-server -n argocd 8080:443
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

# Manually connect with git
# Manually create app-of-apps app in argo ui

``````